# Simpsons in CSS

Simpsons made entirely with CSS without any images! Made by [Chris Pattle](https://github.com/pattle).

Forked from: [https://github.com/pattle/simpsons-in-css](https://github.com/pattle/simpsons-in-css) \
License: Not found, but code available

Changes applied:
- Removed the favicon


# Use Online

- [https://staticapps.gitlab.io/simpsons](https://staticapps.gitlab.io/simpsons)
- [https://pattle.github.io/simpsons-in-css/](https://pattle.github.io/simpsons-in-css/)
- [https://open.lbry.com/@appsinsidelbry:5/simpsons:9](https://open.lbry.com/@appsinsidelbry:5/simpsons:9)


# Deployment on LBRY

Install [lpack-bash](https://gitlab.com/funapplic66/lpack-bash) then run:

```
lpack public simpsons-xyz.lbry
```

Then upload the resulting `.lbry` file to [LBRY](https://lbry.tv).
